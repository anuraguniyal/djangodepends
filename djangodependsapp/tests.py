"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from models import NormalModel, DependentModel

class DependsTest(TestCase):
    
    def test_dependent_default(self):
        """
        Tests that 1 + 1 always equals 2.
        """

        o = DependentModel()
        self.assertEqual(o.number, 10)
        self.assertEqual(o.threshold, 10)
        o.save()
        self.assertEqual(o.threshold, 100)
        
    def test_normal_model(self):
        o = NormalModel()
        o.save()