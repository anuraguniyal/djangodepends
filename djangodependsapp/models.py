from django.db import models
from fields import FieldDepends

class NormalModel(models.Model):
    number = models.IntegerField(null=True, blank=True, default=10)
    threshold = models.IntegerField(null=True, blank=True, default=10,editable=False)
    created_time = models.DateTimeField(auto_now_add=True)
    
class DependentModel(models.Model):

    def threshold_default(model_instance=None):
        if model_instance is None:
            return 10
        return model_instance.number*10
    
    number = models.IntegerField(null=True, blank=True, default=10)
    threshold = FieldDepends(models.IntegerField)(null=True, blank=True, default=threshold_default,editable=False)

    

    